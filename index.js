let mainHeading = document.querySelector('h1');

let subHeading = document.querySelector('h2');

mainHeading.style.textAlign = 'center';

subHeading.style.textAlign = 'center';

let boxOne = document.querySelector('.one');
let boxTwo = document.querySelector('.two');
let boxThree = document.querySelector('.three');
let boxFour = document.querySelector('.four');
let boxFive = document.querySelector('.five');
let boxSix = document.querySelector('.six');
let boxSeven= document.querySelector('.seven');
let boxEight = document.querySelector('.eight');
let boxNine = document.querySelector('.nine');
let boxTen = document.querySelector('.ten');
let boxEleven = document.querySelector('.eleven');
let boxTwelve = document.querySelector('.twelve');

boxOne.addEventListener('click',()=> {

    boxOne.textContent = '1';

    setTimeout(()=>{
        boxOne.textContent = '';
    },5000)
})


boxTwo.addEventListener('click',()=> {

    boxTwo.textContent = '2';

    setTimeout(()=>{
        boxTwo.textContent = '';
    },5000)
})

boxThree.addEventListener('click',()=> {

    boxThree.textContent = '3';

    setTimeout(()=>{
        boxThree.textContent = '';
    },5000)
})

boxFour.addEventListener('click',()=> {

    boxFour.textContent = '4';

    setTimeout(()=>{
        boxFour.textContent = '';
    },5000)
})

boxFive.addEventListener('click',()=> {

    boxFive.textContent = '5';

    setTimeout(()=>{
        boxFive.textContent = '';
    },5000)
})


boxSix.addEventListener('click',()=> {

    boxSix.textContent = '6';

    setTimeout(()=>{
        boxSix.textContent = '';
    },5000)
})


boxSeven.addEventListener('click',()=> {

    boxSeven.textContent = '7';

    setTimeout(()=>{
        boxSeven.textContent = '';
    },5000)
})


boxEight.addEventListener('click',()=> {

    boxEight.textContent = '8';

    setTimeout(()=>{
        boxEight.textContent = '';
    },5000)
})

boxNine.addEventListener('click',()=> {

    boxNine.textContent = '9';

    setTimeout(()=>{
        boxNine.textContent = '';
    },5000)
})


boxTen.addEventListener('click',()=> {

    boxTen.textContent = '10';

    setTimeout(()=>{
        boxTen.textContent = '';
    },5000)
})

boxEleven.addEventListener('click',()=> {

    boxEleven.textContent = '11';

    setTimeout(()=>{
        boxEleven.textContent = '';
    },5000)
})

boxTwelve.addEventListener('click',()=> {

    boxTwelve.textContent = '12';

    setTimeout(()=>{
        boxTwelve.textContent = '';
    },5000)
})

let evdel = document.querySelector('.event-delegation');

evdel.addEventListener('click', (ev)=>{
    if (ev.target.matches('.ed-box')) {
        let num = ev.target.getAttribute('num');
    ev.target.textContent = num;
    setTimeout(()=>{
        ev.target.textContent = ''
    },5000)
    }
})